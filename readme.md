# update_custom_token
## Run time Environment
  Firebase functions
## Purpose
  update custom token with firebase.auth()
  cert.json is from firebase service account
## Request Body
    {
      uid: "firebase-auth-uid"
      metadata: { "key1": "value1", "key2": "value2" }
    }
## Response
    {
      customToken: "here-is-a-custom-token"
    }
## Deployment
    cd functions && yarn install
    cd .. && firebase deploy --only functions
## Local Testing
    firebase serve
