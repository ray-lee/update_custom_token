const functions = require('firebase-functions')
const admin = require('firebase-admin')
const serviceAccount = require('./cert.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://bomo-development.firebaseio.com'
});

exports.update_custom_token = functions.https.onRequest((request, response) => {
  admin.auth().createCustomToken(request.body.uid, request.body.metadata)
    .then((customToken) => {
      return response.send({customToken: customToken})
    })
    .catch((error) => {
      return response.send("Error creating custom token:", error)
    })
})
